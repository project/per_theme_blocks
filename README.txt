
-- SUMMARY --

The Per-Theme Blocks module allows themes to override blocks information.

This is especially helpful to manage several sites with several themes that
need different blocks information such as region, weight or status. 

For a full description of the module, visit the project page:
  http://drupal.org/project/per_theme_blocks

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/per_theme_blocks


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

No configuration needed.


-- WEBMASTERS --

Go to Administer -> Site building -> Blocks

Check the box "Reset blocks" informations then click "Save blocks" to reset the
blocks information of the selected theme (default is current theme).


-- THEMES & MODULES DEVELOPERS --

To allow a given theme to override blocks information, create a function called
YOUR_THEME_blocks_info() in the template.php file of your theme. Create this file
if it does not exist.

The YOUR_THEME_blocks_info() function must return an array that contains blocks
information to override. The format is the same as expected in hook_block() for
'list' operation.
More information about hook_block() is available in the Drupal API pages :
http://api.drupal.org/api/function/hook_block/6

The blocks_per_theme_reset_blocks($theme_key) function allows to programmatically
reset blocks information for a given theme.
It can be useful to refresh the blocks information after adding a new theme, for
example.

The blocks_per_theme_reset_blocks_all() function allows to reset blocks information
for all enabled themes. 


-- EXAMPLE --

E.g. with a theme called "mytheme" :

------------------------------------------------------------------
<?php

function mytheme_blocks_info() {
  return array (
    'user' => array ( 'weight' => '1', 'region' => 'right', ),
    'myblock1' => array ( 'weight' => '2', 'region' => 'right', ),
    'myblock2' => array ( 'weight' => '1', 'region' => 'footer', ),
    'block_to_hide' => array ( 'status' => '0', ),
  );
}
------------------------------------------------------------------


-- TODO / FUTURE --

* documentation into hook_help() may be helpful  
* allow to configure blocks information through admin/build/block/default form
  and allow to configure if THEME_blocks_info() override default information or not.


-- CONTACT --

Maintainer:
* Florian Cathala (florian.cathala) - http://drupal.org/user/47044

This project has been sponsored by:
* Ambika
  Web company located in France, near Paris. Ambika provides Drupal-driven
  services since 2007. Visit http://www.ambika.fr for more information.